/*
 * BB: The portable demo
 *
 * (C) 1997 by AA-group (e-mail: aa@horac.ta.jcu.cz)
 *
 * 3rd August 1997
 * version: 1.2 [final3]
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public Licences as by published
 * by the Free Software Foundation; either version 2; or (at your option)
 * any later version
 *
 * This program is distributed in the hope that it will entertaining,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILTY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Publis License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.
 * 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <ctype.h>
#include <unistd.h>
#include "timers.h"
#include "bb.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <sys/prctl.h>

char *song_command;
pid_t child_thread;

int
load_song (char *name)
{
  free(song_command);
  song_command = malloc (strlen (name) + strlen(SOUNDDIR)+23);
  sprintf(song_command, "mpv %s/%s > /dev/null 2>&1",SOUNDDIR, name);
}

void
stop ()
{
  
  // Send sigterm to the running thread
  if (child_thread)
    killpg(child_thread, 15);
}

// What are lambdas
void
stop_and_exit ()
{
    stop();
    exit(1);
}

void
play ()
{
  // Stop any previously running songs
  // before starting anything new
  stop();
  int result = fork();
  if (result == 0) 
    {
      // set a new pgrp so we can clean up easier
      setpgrp();

      execl("/bin/sh", "sh", "-c", song_command, (char *) NULL);

    }
  else 
    {
      child_thread = result;
    }

}

#ifdef HAVE_LIBMIKMOD
struct table
{
  int v;
  char *c;
};
static struct table stable[] = { {DMODE_16BITS, "16 bit output"},
{DMODE_STEREO, "Stereo output"},
{DMODE_SOFT_MUSIC, "Process music via software mixer"},
{DMODE_HQMIXER, "Use high-quality (slower) software mixer"},
{DMODE_SURROUND, "Surround sound"},
{DMODE_INTERP, "Interpolation"},
{DMODE_REVERSE, "Reverse Stereo"},
{0, NULL}
};

int cont;
int srate;
void
ptable ()
{
  int i;
  for (i = 0; stable[i].c; i++)
    aa_printf (context, 0, i, AA_SPECIAL, "%i:%s - %-40s", i,
	       md_mode & stable[i].v ? "Yes" : "No ", stable[i].c);
  aa_printf (context, 0, i, AA_SPECIAL,
	     "%i:Sample rate: %i                            ", i, md_mixfreq);
  aa_printf (context, 0, i + 1, AA_SPECIAL,
	     "%i:Continue                                      ", i + 1);
  srate = i;
  cont = i + 1;
}
#endif

int
main (int argc, char *argv[])
{
  int retval;

  // Setup child cleanup on bb exit
  atexit(stop);
  signal(SIGINT,stop_and_exit);
  signal(SIGTERM,stop_and_exit);
  signal(SIGKILL,stop_and_exit);

  bbinit (argc, argv);
  aa_resize(context);
  retval = bb ();

  return retval;
}
